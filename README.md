# SFL Javascript Assignment

## What's this?

The project is a single page application without using any modern framework and libraries such as React, Vue, etc.

I used web components and shadow-dom for ceating reusable elements.

## Installing dependencies

How ever the project do not use any dependencies, but the are some which is required for development. Development dependencies can be installed by one of following commands.

```bash
npm install
```

or with `Yarn`

```bash
yarn
```

## Dev Mode

Executing following command will run the project in development mode

```bash
npm run dev
```

or with `Yarn`

```bash
yarn dev
```

## Building bundle

For making bundle files use following command.

```bash
npm run build
```

or with `Yarn`

```bash
yarn build
```