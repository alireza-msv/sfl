const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCssAssetPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const WebpackBar = require('webpackbar');

const PORT = process.env.port || 3000;
const ENV = process.env.NODE_ENV || 'development';
const IS_PROD = ENV === 'production';

module.exports = {
  entry: {
    app: path.resolve(__dirname, 'src/index.js'),
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: `bundle-[${IS_PROD ? 'hash' : ''}].js`,
  },
  resolve: {
    extensions: ['.js', '.json', 'scss'],
    alias: {
      '~': path.resolve(__dirname, './src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        exclude: path.resolve(__dirname, 'src'),
        use: 'source-map-loader',
      },
      {
        test: /\.js?$/,
        use: 'babel-loader',
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !IS_PROD,
            },
          },
        ],
      },
    ],
  },
  plugins: ([
    new WebpackBar(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(ENV),
    }),
    new HtmlWebpackPlugin({
      template: './public/index.ejs',
      minify: false,
    }),
  ]),
  optimization: {
    minimize: IS_PROD,
    minimizer: [
      new TerserPlugin(),
      new OptimizeCssAssetPlugin({}),
      new UglifyJSPlugin({
        uglifyOptions: {
          compress: {
            drop_console: true,
          },
        },
      }),
    ],
  },
  stats: { colors: true },
  devtool: IS_PROD ? false : 'source-map',
  devServer: {
    port: process.env.PORT || 3000,
    host: 'localhost',
    publicPath: '/',
    contentBase: './src',
    historyApiFallback: true,
    open: false,
    openPage: `http://localhost:${PORT}`,
  },
};
