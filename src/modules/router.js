class Router {
  constructor(routes) {
    this.routes = routes;

    this.onHistoryPopstate = this.onHistoryPopstate.bind(this);
    this.pushState = this.pushState.bind(this);

    this.originalPushState = window.History.prototype.pushState;

    window.History.prototype.pushState = this.pushState;
    window.addEventListener('popstate', this.onHistoryPopstate);
  }

  pushState(data, title, url) {
    if (url !== window.location.pathname) {
      this.originalPushState.call(window.history, data, title, url);
      this.renderRoute();
    }
  }

  onHistoryPopstate() {
    this.renderRoute();
  }

  findRoute() {
    const { pathname } = window.location;
    const currentPath = (pathname.endsWith('/') ? pathname.substring(0, pathname.length - 1) : pathname).toLowerCase();

    return this.routes.find((r) => {
      if (r.exact === true && currentPath === r.path) {
        return true;
      }

      const pattern = r.path.replace(/([:].+\/?)/g, '.+/?');
      const reg = new RegExp(`^${pattern}$`);

      return reg.test(currentPath);
    });
  }

  renderRoute() {
    const route = this.findRoute();

    if (route) {
      this.mountNode.innerHTML = '';
      const node = document.createElement(route.tag);
      this.mountNode.appendChild(node);
    }
  }

  render(mountNode) {
    this.mountNode = mountNode;
    this.renderRoute();
  }
}

export default Router;
