const BASE_URL = 'https://api.github.com';

const makeRequest = (path, options = {}) => (
  fetch(`${BASE_URL}/${path}`, {
    ...options,
    headers: {
      ...(options.headers || {}),
      accept: 'application/vnd.github.v3+json',
    },
  })
    .then((res) => res.json())
);

export const get = (path, params = {}) => {
  const query = new URLSearchParams(params);
  const options = {
    method: 'get',
  };

  return makeRequest(`${path}${query ? `?${query}` : ''}`, options);
};

export const post = (path, data = {}) => (
  makeRequest(path, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: { 'Content-type': 'application/json; charset=UTF-8' },
  })
);
