import { get } from './http';
import { toCamelCaseObject } from '../utils/camelCase';

/**
 * gets users repositories
 * @param {String} user username
 * @returns {Promise<Object[]>}
 * @public
 */
export const getUserRepos = async (user, page) => {
  const params = { page, per_page: 10 };
  const res = await get(`users/${user}/repos`, params);

  return res.map((repo) => toCamelCaseObject(repo));
};

/**
 * searchs for repositories with given query
 * @param {String} searchQuery query
 * @param {Number} page start page
 * @returns {Promise<{total: number, repos: Object[]}>}
 * @public
 */
export const searchRepos = async (searchQuery, page = 1) => {
  const params = {
    page,
    per_page: 10,
    q: encodeURIComponent(searchQuery),
  };
  const res = await get('search/repositories', params);

  if (res.items.length) {
    const repos = res.items.map((repo) => toCamelCaseObject(repo));

    return {
      total: res.total_count,
      repos,
    };
  }

  return {
    total: 0,
    repos: [],
  };
};

export default {
  getUserRepos,
};
