import { get } from './http';
import { toCamelCaseObject } from '../utils/camelCase';

/**
 * gets list of Github users
 * @returns {Promise<Object>}
 * @public
 */
export const getUsers = async (page = 1) => {
  const params = {
    per_page: 10,
    page,
  };

  const res = await get('users', params);
  const users = res.map((user) => toCamelCaseObject(user));

  return users;
};

/**
 * search in Github users
 * @returns {Promise<Object>}
 * @public
 */
export const searchUsers = async (searchQuery, page = 1) => {
  const params = {
    per_page: 10,
    page,
    q: encodeURIComponent(searchQuery),
  };

  const res = await get('search/users', params);
  if (res.items.length) {
    const users = res.items.map((user) => toCamelCaseObject(user));

    return {
      users,
      total: res.total_count,
    };
  }

  return {
    users: [],
    total: 0,
  };
};

/**
 * gets user details from Github
 * @param {String} username user perma
 * @returns {Promise<Object>}
 * @public
 */
export const getUserDetails = async (username) => {
  const res = await get(`users/${username}`);

  return toCamelCaseObject(res);
};
