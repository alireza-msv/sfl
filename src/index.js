import Router from './modules/router';
import './components';
import './pages';
import './globalStyles.scss';

const rootNode = document.getElementById('app');

const routes = [
  {
    path: '',
    tag: 'sfl-pages-home',
    exact: true,
  },
  {
    path: '/users',
    tag: 'sfl-pages-users',
    exact: true,
  },
  {
    path: '/users/:user',
    tag: 'sfl-pages-user-details',
  },
  {
    path: '/repos',
    tag: 'sfl-pages-repos',
    axact: true,
  },
];

const router = new Router(routes);

router.render(rootNode);
