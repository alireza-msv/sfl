/**
 * converts source string to camelCase
 * @param {String} source source string
 * @returns {String}
 * @private
 */
const toCamelCase = (source) => {
  let camelCased = '';
  let shouldUpperCase = false;

  for (let i = 0; i < source.length; i += 1) {
    const isDelimiter = source[i] === '_' || source[i] === '-' || source[i] === ' ';

    if (isDelimiter) {
      shouldUpperCase = true;
    } else if (shouldUpperCase) {
      if (!isDelimiter) {
        if (camelCased) {
          camelCased += source[i].toUpperCase();
        } else {
          camelCased = source[i].toLowerCase();
        }

        shouldUpperCase = false;
      }
    } else {
      camelCased += camelCased ? source[i] : source[i].toLowerCase();
    }
  }

  return camelCased;
};

/**
 * converts object keys from snake_case to camelCase
 * @param {Object} source source object
 * @return {Object}
 * @public
 */
export const toCamelCaseObject = (source) => Object
  .keys(source)
  .reduce((acc, key) => {
    acc[toCamelCase(key)] = source[key];
    return acc;
  }, {});

export default {
  toCamelCaseObject,
};
