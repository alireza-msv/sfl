const html = `
<form id="form" action="#">
  <input id="input" type="search" class="input">
</button>
`;

const css = `
.input {
  padding: 4px 12px;
  background-color: transparent;
  font-size: 14px;
  font-weight: 500;
  border: 1px solid #ccc;
  border-radius: 5px;
  height: 40px;
  width: 100%;
}

.input:focus {
  outline: none;
  box-shadow: 0 0 0 3px rgba(30, 153, 227, 0.44);
  border-color: transparent;
}
`;

class SearchBox extends HTMLElement {
  constructor() {
    super();

    this.onSubmitt = this.onSubmitt.bind(this);

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);
  }

  connectedCallback() {
    const placeholder = this.getAttribute('placeholder');
    this.root.getElementById('input').setAttribute('placeholder', placeholder);

    this.root.getElementById('form').addEventListener('submit', this.onSubmitt);
  }

  disconnectedCallback() {
    this.root.getElementById('form').removeEventListener('submit', this.onSubmitt);
  }

  onSubmitt(e) {
    e.preventDefault();
    const input = this.root.getElementById('input');

    const event = new Event('search');
    event.searchTerm = input.value;
    this.dispatchEvent(event);
  }
}

window.customElements.define('sfl-search-box', SearchBox);
