const html = `
  <div class="repo-item">
    <div class="repo-name"></div>
    <div class="repo-url"></div>
  </div>
`;

const css = `
.repo-item {
  box-sizing: border-box;
  border: 1px solid #ccc;
  border-radius: 5px;
  margin-bottom: 16px;
  width: 100%;
  padding: 8px 16px;
}

.repo-name {
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 8px;
}

.repo-url {
  color: #999;
  font-size: 12px;
}
`;

class RepoItem extends HTMLElement {
  static get observedAttributes() {
    return ['name', 'url'];
  }

  constructor() {
    super();

    this.onNameChange = this.onNameChange.bind(this);
    this.onUrlChange = this.onUrlChange.bind(this);

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);
  }

  connectedCallback() {
    const name = this.getAttribute('name');
    const url = this.getAttribute('url');

    this.onNameChange(name);
    this.onUrlChange(url);
  }

  attributeChangedCallback(name, _, newValue) {
    switch (name) {
      case 'name':
        this.onNameChange(newValue);
        break;
      case 'url':
        this.onUrlChange(newValue);
        break;
      default:
      // do nothing
    }
  }

  onNameChange(name) {
    this.root.querySelector('.repo-name').textContent = name;
  }

  onUrlChange(url) {
    this.root.querySelector('.repo-url').textContent = url;
  }
}

window.customElements.define('sfl-repo-item', RepoItem);
