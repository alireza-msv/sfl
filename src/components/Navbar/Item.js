const html = `
  <li>
    <slot></slot>
  </li>
`;

const css = `
li {
  height: 60px;
  padding: 0 12px;
  font-size: 18px;
  font-weight: 400;
  display: inline-flex;
  align-items: center;
}
`;

class Item extends HTMLElement {
  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.textContent = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);
  }
}

window.customElements.define('sfl-navbar-item', Item);
