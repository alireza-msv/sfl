import './Item';

const html = `
<nav>
  <ul>
    <slot></slot>
  </ul>
</nav>
`;

const css = `
nav {
  background-color: #1e99e3;
  height: 60px;
  padding: 0 20px;
}

ul {
  display: flex;
  align-items: center;
  list-style: none;
  margin: 0;
  padding: 0;
}
`;

class Navbar extends HTMLElement {
  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    const rootShadow = this.attachShadow({ mode: 'open' });
    rootShadow.appendChild(style);
    rootShadow.appendChild(template.content);
  }
}

window.customElements.define('sfl-navbar', Navbar);
