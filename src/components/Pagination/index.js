const html = `
<div class="container">
  <button id="prev" class="button">
    <sfl-icon name="chevron-left" class="icon"><sfl-icon>
  </button>
  <div class="text">
    Page 1
  </div>
  <button id="next" class="button">
    <sfl-icon name="chevron-right" class="icon"><sfl-icon>
  </button>
</div>
`;

const css = `
.container {
  display: inline-flex;
  align-items: center;
}

.button {
  width: 28px;
  height: 28px;
  border-radius: 50%;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
}

.icon {
  width: 8px;
  line-height: 0;
  display: block;
}

.text {
  width: 80px;
  text-align: center;
}
`;

class Pagination extends HTMLElement {
  static get observedAttributes() {
    return ['page', 'max'];
  }

  constructor() {
    super();

    this.onPageChange = this.onPageChange.bind(this);
    this.onMaxChange = this.onMaxChange.bind(this);
    this.onNextClick = this.onNextClick.bind(this);
    this.onPrevClick = this.onPrevClick.bind(this);

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);
  }

  connectedCallback() {
    const page = parseInt(this.getAttribute('page'), 10);
    const max = parseInt(this.getAttribute('max'), 10);

    this.onPageChange(page);
    this.onMaxChange(max);

    const next = this.root.getElementById('next');
    const prev = this.root.getElementById('prev');

    next.addEventListener('click', this.onNextClick);
    prev.addEventListener('click', this.onPrevClick);
  }

  attributeChangedCallback(name, _, newValue) {
    switch (name) {
      case 'page':
        this.onPageChange(parseInt(newValue, 10));
        break;
      case 'max':
        this.onMaxChange(parseInt(newValue, 10));
        break;
      default:
      // do nothing
    }
  }

  onPageChange(page) {
    const max = parseInt(this.getAttribute('max'), 10);
    this.updatePagination(page, max);
  }

  onMaxChange(max) {
    const page = parseInt(this.getAttribute('page'), 10);
    this.updatePagination(page, max);
  }

  updatePagination(page, max) {
    const next = this.root.getElementById('next');
    const prev = this.root.getElementById('prev');

    if (page === max) {
      next.setAttribute('disabled', true);
    } else {
      next.removeAttribute('disabled');
    }

    if (page === 1) {
      prev.setAttribute('disabled', true);
    } else {
      prev.removeAttribute('disabled', true);
    }

    this.root.querySelector('.text').textContent = `${page} of ${max}`;
  }

  onNextClick() {
    const event = new Event('next');
    this.dispatchEvent(event);
  }

  onPrevClick() {
    const event = new Event('prev');
    this.dispatchEvent(event);
  }
}

window.customElements.define('sfl-pagination', Pagination);
