const html = `
<a>
  <slot></slot>
</a>
`;

const css = `
a {
  text-decoration: none;
}

a:visited {
  color: inherit;
}
`;

class RouterLink extends HTMLElement {
  static get observedAttributes() {
    return ['href'];
  }

  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);

    this.navigateTo = this.navigateTo.bind(this);
  }

  connectedCallback() {
    this.root.querySelector('a').addEventListener('click', this.navigateTo);
    this.root.querySelector('a').setAttribute('href', this.getAttribute('href'));
  }

  disconnectedCallback() {
    this.root.querySelector('a').removeEventListener('click', this.navigateTo);
  }

  attributeChangedCallback(name, _, newValue) {
    if (name === 'href') {
      this.root.querySelector('a').setAttribute('href', newValue);
    }
  }

  navigateTo(e) {
    e.preventDefault();

    window.history.pushState({}, '', this.getAttribute('href'));
  }
}

window.customElements.define('sfl-router-link', RouterLink);
