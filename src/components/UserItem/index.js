const html = `
<div class="user">
  <figure class="avatar">
    <img>
  </figure>
  <div class="username"></div>
</div>
`;

const css = `
.user {
  padding: 20px;
  box-sizing: border-box;
  margin-bottom: 20px;
}

.avatar {
  width: 180px;
  height: 180px;
  border-radius: 50%;
  overflow: hidden;
  margin-bottom: 15px;
  margin-right: auto;
  margin-left: auto;
}

.avatar img {
  max-width: 100%;
  max-height: 100%;
}

.username {
  font-weight: 600;
  font-size: 24px;
  text-align: center;
}
`;

class UserItem extends HTMLElement {
  static get observedAttributes() {
    return ['avatar', 'name'];
  }

  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);

    this.onNameChange = this.onNameChange.bind(this);
    this.onAvatarChange = this.onAvatarChange.bind(this);
  }

  connectedCallback() {
    const name = this.getAttribute('name');
    const avatar = this.getAttribute('avatar');

    this.onNameChange(name);
    this.onAvatarChange(avatar);
  }

  attributeChangedCallback(name, _, newValue) {
    if (name === 'name') {
      this.onNameChange(newValue);
    } else if (name === 'avatar') {
      this.onAvatarChange(newValue);
    }
  }

  onNameChange(value) {
    this.root.querySelector('.username').textContent = value;
  }

  onAvatarChange(value) {
    this.root.querySelector('.avatar img').src = value;
  }
}

window.customElements.define('sfl-user-item', UserItem);
