const html = `
<button class="btn">
  <div class="icon">
    <sfl-icon name="arrow-left"></sfl-icon>
  </div>
  <div class="text">
    <slot></slot>
  </div>
</button>
`;

const css = `
.btn {
  display: inline-flex;
  align-items: center;
  padding: 4px 12px;
  cursor: pointer;
  border: none;
  background-color: transparent;
  font-size: 14px;
  font-weight: 500;
}

.icon {
  width: 16px;
  height: 16px;
}

.text {
  padding-left: 12px;
}
`;

class BackButton extends HTMLElement {
  constructor() {
    super();

    this.onClick = this.onClick.bind(this);

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);
  }

  connectedCallback() {
    this.root.querySelector('.btn').addEventListener('click', this.onClick);
  }

  disconnectedCallback() {
    this.root.querySelector('.btn').removeEventListener('click', this.onClick);
  }

  onClick() {
    window.history.back();
  }
}

window.customElements.define('sfl-back-button', BackButton);
