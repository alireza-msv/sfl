import { getUserRepos } from '../../api/repos';

const html = `
  <section>
    <div class="repos-container">
      <div class="repos-title">
        Repositories
      </div>
      <div class="pagination">
        <sfl-pagination id="pagination" page="1" max="1"></sfl-pagination>
      </div>
    </div>
    <div id="repos">
    </div>
  </section>
`;

const css = `
a {
  text-decoration: none;
  color: inherit;
}

.repos-container {
  display: flex;
  margin-bottom: 16px;
}

.repos-title {
  flex-grow: 1;
  font-size: 18px;
  font-weight: 500;
}

.pagination {
  flex: 0 0 150px;
  max-width: 150px;
}
`;

class ReposList extends HTMLElement {
  static get observedAttributes() {
    return ['total-repos'];
  }

  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    this.root = this.attachShadow({ mode: 'open' });

    this.root.appendChild(style);
    this.root.appendChild(template.content);

    this.reposNode = this.root.querySelector('#repos');
    this.pagination = this.root.querySelector('#pagination');

    this.onNextPagePaginate = this.onNextPagePaginate.bind(this);
    this.onPrevPagePaginate = this.onPrevPagePaginate.bind(this);

    this.state = {
      page: 1,
      totalPages: 0,
    };
  }

  get page() {
    return this.state.page;
  }

  set page(value) {
    this.state.page = value;
    this.getRepos();
    this.pagination.setAttribute('page', value);
  }

  get totalPages() {
    return this.state.totalPages;
  }

  set totalPages(value) {
    this.state.totalPages = value;
    this.pagination.setAttribute('max', value);
  }

  connectedCallback() {
    this.pagination.addEventListener('next', this.onNextPagePaginate);
    this.pagination.addEventListener('prev', this.onPrevPagePaginate);

    this.getRepos();
  }

  attributeChangedCallback(name, _, newValue) {
    if (name === 'total-repos') {
      const reposCount = parseInt(newValue, 10);
      this.totalPages = reposCount % 10 ? Math.round(reposCount / 10) : reposCount / 10;
    }
  }

  async getRepos() {
    this.reposNode.textContent = 'Loading Repositories';

    const user = this.getAttribute('user');
    const repos = await getUserRepos(user, this.page);

    this.reposNode.textContent = '';

    repos.forEach((r) => {
      const node = this.createRepoItem(r.name, r.svnUrl);
      this.reposNode.appendChild(node);
    });
  }

  createRepoItem(name, url) {
    const node = document.createElement('sfl-repo-item');
    node.setAttribute('name', name);
    node.setAttribute('url', url);

    const link = document.createElement('a');
    link.setAttribute('href', url);
    link.setAttribute('target', '_blank');
    link.setAttribute('rel', 'noopener nofollower');
    link.append(node);

    return link;
  }

  onNextPagePaginate() {
    this.page += 1;
  }

  onPrevPagePaginate() {
    this.page -= 1;
  }
}

window.customElements.define('sfl-repos-list', ReposList);
