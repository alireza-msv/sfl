import { getUsers, searchUsers } from '../../api/users';

const html = `
<section class="page">
  <header class="header">
    <div class="search-box">
      <sfl-search-box id="search" placeholder="Search for users"></sfl-search-box>
    </div>
    <div class="pagination">
      <sfl-pagination id="pagination" page="1" max="1"></sfl-pagination>
    </div>
  </header>
  <div id="users_list" class="list">
    Loading
  </div>
</section>
`;

const css = `
.page {
  padding: 25px;
}

.header {
  padding-bottom: 20px;
  display: flex;
  justify-content: space-between;
}

.search-box {
  flex: 0 0 40%;
  max-width: 40%;
}

.pagination {
  flex: 0 0 150px;
  max-width: 150px;
  text-align: right;
}

.list {
  display: flex;
  flex-wrap: wrap;
}

.user {
  flex: 0 0 20%;
  max-width: 20%;
}
`;

class Users extends HTMLElement {
  constructor() {
    super();

    const root = document.createElement('section');
    root.innerHTML = html;

    const style = document.createElement('style');
    style.textContent = css;

    const rootShadow = this.attachShadow({ mode: 'open' });
    rootShadow.appendChild(style);
    rootShadow.appendChild(root);

    this.root = root;
    this.getUsersList();

    this.state = {
      page: 1,
      searchTerm: '',
      total: 1,
    };

    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onNextPage = this.onNextPage.bind(this);
    this.onPrevPage = this.onPrevPage.bind(this);
  }

  get page() {
    return this.state.page;
  }

  set page(value) {
    this.state.page = value;
    this.getUsersList(this.state.searchTerm, value);
    this.pagination.setAttribute('page', value);
  }

  get total() {
    return this.state.total;
  }

  set total(value) {
    this.state.total = value;

    const totalPages = (value % 10) ? Math.round(value / 10) + 1 : value / 10;
    this.pagination.setAttribute('max', totalPages);
  }

  connectedCallback() {
    this.root.querySelector('#search').addEventListener('search', this.onSearchSubmit);
    this.pagination = this.root.querySelector('#pagination');

    this.pagination.addEventListener('next', this.onNextPage);
    this.pagination.addEventListener('prev', this.onPrevPage);
  }

  async getUsersList(searchTerm, page = 1) {
    let users;

    if (searchTerm) {
      const response = await searchUsers(searchTerm, page);
      users = response.users;
      this.total = response.total;
    } else {
      users = await getUsers(page);
    }

    const listNode = this.root.querySelector('#users_list');

    if (users.length) {
      listNode.innerHTML = '';

      users.forEach((user) => {
        const node = this.createUserItem(user);
        listNode.appendChild(node);
      });
    } else {
      listNode.innerHTML = 'Nothing found';
    }
  }

  onSearchSubmit(e) {
    this.state.searchTerm = e.searchTerm;
    this.page = 1;
  }

  createUserItem(user) {
    const node = document.createElement('sfl-user-item');
    node.setAttribute('name', user.login);
    node.setAttribute('avatar', user.avatarUrl);

    const link = document.createElement('sfl-router-link');
    link.setAttribute('href', `/users/${user.login}`);
    link.append(node);

    const container = document.createElement('div');
    container.classList.add('user');
    container.appendChild(link);

    return container;
  }

  onNextPage() {
    this.page += 1;
  }

  onPrevPage() {
    this.page -= 1;
  }
}

window.customElements.define('sfl-pages-users', Users);
