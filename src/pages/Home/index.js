const html = `
<section>
  <h1>
    Simple GitHub browser
  </h1>
</section>
`;

const css = `
section {
  height: calc(100vh - 60px);
  display: flex;
  align-items: center;
  justify-content: center;
}

h1 {
  font-size: 50px;
  font-weight: 200;
  font-style: italic;
  margin: 0;
  padding: 0;
  text-align: center;
  text-transform: uppercase;
}
`;

class Home extends HTMLElement {
  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.innerHTML = css;

    const rootShadow = this.attachShadow({ mode: 'open' });
    rootShadow.appendChild(style);
    rootShadow.appendChild(template.content);
  }
}

window.customElements.define('sfl-pages-home', Home);
