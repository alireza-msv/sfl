import { getUserDetails } from '../../api/users';

const html = `
<section class="page">
  <div>
    <sfl-back-button>
      Back to users
    </sfl-back-button>
  </div>
  <div class="info-row">
    <div class="avatar-col"></div>
    <div class="bio-col"></div>
  </div>
  <div class="repos"></div>
</section>
`;

const css = `
.page {
  padding: 25px;
}

.info-row {
  display: flex;
}

.avatar-col {
  flex: 0 0 250px;
  max-width: 250px;
}

.bio-col {
  flex-grow: 1;
}
`;

class Users extends HTMLElement {
  constructor() {
    super();

    const template = document.createElement('template');
    template.innerHTML = html;

    const style = document.createElement('style');
    style.textContent = css;

    this.root = this.attachShadow({ mode: 'open' });
    this.root.appendChild(style);
    this.root.appendChild(template.content);

    this.getUserInfo();
  }

  async getUserInfo() {
    const { pathname } = window.location;
    const endIndex = pathname.indexOf('/', 7);
    const username = pathname
      .replace('/users/', '')
      .substring(0, endIndex !== -1 ? endIndex : undefined);

    const user = await getUserDetails(username);

    this.render(user);
  }

  render(user) {
    const avatar = this.createUserAvatar(user.name || user.login, user.avatarUrl);
    const bio = this.createBioNode(user.bio || 'The user has no bio');
    const repos = this.createReposListNode(user.login, user.publicRepos);

    this.root.querySelector('.avatar-col').append(avatar);
    this.root.querySelector('.bio-col').append(bio);
    this.root.querySelector('.repos').append(repos);
  }

  createUserAvatar(name, avatar) {
    const node = document.createElement('sfl-user-item');
    node.setAttribute('name', name);
    node.setAttribute('avatar', avatar);

    return node;
  }

  createBioNode(bio) {
    const node = document.createElement('p');
    node.setAttribute('slot', 'bio');
    node.textContent = bio;

    return node;
  }

  createReposListNode(user, totalRepos) {
    const node = document.createElement('sfl-repos-list');
    node.setAttribute('user', user);
    node.setAttribute('total-repos', totalRepos);
    return node;
  }
}

window.customElements.define('sfl-pages-user-details', Users);
