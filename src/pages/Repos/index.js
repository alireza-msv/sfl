import { searchRepos } from '../../api/repos';

const html = `
<section class="page">
  <header class="header">
    <div class="search-box">
      <sfl-search-box id="search" placeholder="Search for users"></sfl-search-box>
    </div>
    <div class="pagination">
      <sfl-pagination id="pagination" page="1" max="1"></sfl-pagination>
    </div>
  </header>
  <div id="repos_list" class="list">
    Type repo name in the search box and press Enter
  </div>
</section>
`;

const css = `
.page {
  padding: 25px;
}

.header {
  padding-bottom: 20px;
  display: flex;
  justify-content: space-between;
}

.search-box {
  flex: 0 0 40%;
  max-width: 40%;
}

.pagination {
  flex: 0 0 150px;
  max-width: 150px;
  text-align: right;
}

a, a:visited {
  color: inherit;
  text-decoration: none;
}
`;

class Repos extends HTMLElement {
  constructor() {
    super();

    const root = document.createElement('section');
    root.innerHTML = html;

    const style = document.createElement('style');
    style.textContent = css;

    const rootShadow = this.attachShadow({ mode: 'open' });
    rootShadow.appendChild(style);
    rootShadow.appendChild(root);

    this.root = root;

    this.state = {
      page: 1,
      searchTerm: '',
      total: 1,
    };

    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onNextPage = this.onNextPage.bind(this);
    this.onPrevPage = this.onPrevPage.bind(this);
  }

  get page() {
    return this.state.page;
  }

  set page(value) {
    this.state.page = value;
    this.getReposList(this.state.searchTerm, value);
    this.pagination.setAttribute('page', value);
  }

  get total() {
    return this.state.total;
  }

  set total(value) {
    this.state.total = value;

    const totalPages = (value % 10) ? Math.round(value / 10) + 1 : value / 10;
    const pagination = this.root.querySelector('#pagination');
    pagination.setAttribute('max', totalPages);
  }

  connectedCallback() {
    this.root.querySelector('#search').addEventListener('search', this.onSearchSubmit);
    this.pagination = this.root.querySelector('#pagination');

    this.pagination.addEventListener('next', this.onNextPage);
    this.pagination.addEventListener('prev', this.onPrevPage);
  }

  async getReposList(searchTerm, page = 1) {
    let repos = [];

    if (searchTerm) {
      const response = await searchRepos(searchTerm, page);
      repos = response.repos;
      this.total = response.total;
    }

    const listNode = this.root.querySelector('#repos_list');

    if (repos.length) {
      listNode.innerHTML = '';

      repos.forEach((repo) => {
        const node = this.createRepoItem(repo);
        listNode.appendChild(node);
      });
    } else {
      listNode.innerHTML = 'Nothing found';
    }
  }

  onSearchSubmit(e) {
    this.state.searchTerm = e.searchTerm;
    this.page = 1;
  }

  createRepoItem(repo) {
    const node = document.createElement('sfl-repo-item');
    node.setAttribute('name', repo.name);
    node.setAttribute('url', repo.svnUrl);

    const link = document.createElement('a');
    link.setAttribute('href', repo.svnUrl);
    link.setAttribute('target', '_blank');
    link.setAttribute('rel', 'noopener nofollower');
    link.append(node);

    const container = document.createElement('div');
    container.appendChild(link);

    return container;
  }

  onNextPage() {
    this.page += 1;
  }

  onPrevPage() {
    this.page -= 1;
  }
}

window.customElements.define('sfl-pages-repos', Repos);
